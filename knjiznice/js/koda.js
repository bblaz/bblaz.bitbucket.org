var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}



/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 
function generirajPodatke(stPacienta) {
 sessionId = getSessionId();
 
 var ime; var priimek; var datumRojstva; var telesnaTeza; var telesnaVisina;

		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		
		if (stPacienta == 1) {
			ime = "John";
			priimek = "Tenisac";
			datumRojstva = "1975-12-12";
			telesnaTeza = 75;
			telesnaVisina = 180;
		}
		
		else if (stPacienta == 2) {
			ime = "Old";
			priimek = "Lady";
			datumRojstva = "1940-10-10";
			telesnaTeza = 70;
			telesnaVisina = 140;
		}
		
		else if (stPacienta == 3) {
			ime = "Captain";
			priimek = "America";
			datumRojstva = "2006-11-12";
			telesnaTeza = 80;
			telesnaVisina = 209;
		}
		
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreirani EHR ID bolnikov.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                    $("#preberiObstojeciEHRid")[0][stPacienta].value=ehrId;
		                    dodajMeritveVitalnihZnakov(ehrId, telesnaVisina, telesnaTeza);
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});

  
}

function generirajVseTriPaciente(){
	generirajPodatke(1);
	generirajPodatke(2);
	generirajPodatke(3);
}

/**
 * Doda podatke za posameznega pacienta na EHR Scape
 * 
 */
 
 function dodajMeritveVitalnihZnakov(ehrId, telesnaVisina, telesnaTeza) {
	sessionId = getSessionId();

	var datumInUra ;
	var telesnaTemperatura ;
	var sistolicniKrvniTlak ;
	var diastolicniKrvniTlak ;
	var nasicenostKrviSKisikom ;
	var merilec ;

		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        console.log("Uspešno zapolnjeni podatki");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

/*
*	Funkcija, ki izpiše podatke trenutno izbranega ehrIdja
*
*/

function preberiEHRPodatkeObstojecegaBolnika() {
	sessionId = getSessionId();
	var ehrId = $("#preberiEHRid").val();
	
	

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
        		$("#vnesiIme").val(party.firstNames);
        		$("#vnesiPriimek").val(party.lastNames);
        		var letoRojstva = party.dateOfBirth.split("-");
        		var starost = 2018 - letoRojstva[0];
        		$("#vnesiStarost").val(starost);
        		
        		$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "weight",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	for (var i in res) {
					    		$("#vnesiTezo").val(res[i].weight);
					    	}
					    } 
        			
        		});
        		
        		$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "height",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	for (var i in res) {
					    		$("#vnesiVisino").val(res[i].height);
					    	}
					    } 
        			
        		});
          
			},
			error: function(err) {
				console.log("Error pri branju");
			}
		});
	}
}

function preberiEHRPodatkePoljubnegaBolnika() {
	sessionId = getSessionId();
	var ehrId = $("#preberiEHRid2").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo1").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
        		$("#vnesiIme").val(party.firstNames);
        		$("#vnesiPriimek").val(party.lastNames);
        		var letoRojstva = party.dateOfBirth.split("-");
        		var starost = 2018 - letoRojstva[0];
        		$("#vnesiStarost").val(starost);
        		
        		$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "weight",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	for (var i in res) {
					    		$("#vnesiTezo").val(res[i].weight);
					    	}
					    } 
        			
        		});
        		
        		$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "height",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	for (var i in res) {
					    		$("#vnesiVisino").val(res[i].height);
					    	}
					    } 
        			
        		});
          
			},
			error: function(err) {
				console.log("Error pri branju");
			}
		});
	}
}

function izracunVO2max() {
	var visina = parseInt($("#vnesiVisino").val());
	var teza =  parseInt($("#vnesiTezo").val());
	var starost = parseInt($("#vnesiStarost").val());
	
	if(isNaN(visina) || isNaN(teza) || isNaN(starost)) {
		$("#napisiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim dodajte podatke o bolniku!");
	} else {

		var VO2maxVisina = -0.0282*(starost)+0.0205*(visina)+0.3200;
		var VO2maxTeza = -0.0296*(starost)+0.0167*(teza)+2.6538;
		var VO2maxSkupni = (VO2maxVisina+VO2maxTeza)/2;
		var max = parseFloat(VO2maxSkupni).toFixed(2);
		var VO2maxVMililitrih = parseFloat((VO2maxSkupni*1000)/teza).toFixed(2);
		var maxim = parseInt(VO2maxVMililitrih);
		$("#napisiVO2max").html("Absolutni VO2max znaša: " + max + " L/min.");
		$("#piechart").data('easyPieChart').update(maxim);
		$(".paj").html(VO2maxVMililitrih);
		
	}
	
}



$(document).ready(function() {
	
	//Napolni okence EHR ID pri izbiri iz padajočega menija
	$('#preberiObstojeciEHRid').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});
	
	
	// Pie chart
	$(function() {
        $("#piechart").easyPieChart({
            
        });
    });
    
    //Master detail
    $("#klikni").click(function() {
    	$("#menu").slideToggle(500);
    });
    $("#klikni1").click(function() {
    	$("#menu1").slideToggle(500);
    });
    $("#klikni2").click(function() {
    	$("#menu2").slideToggle(500);
    });
    $("#klikni3").click(function() {
    	$("#menu3").slideToggle(500);
    });
    $("#klikni4").click(function() {
    	$("#menu4").slideToggle(500);
    });
    $("#klikni5").click(function() {
    	$("#menu5").slideToggle(500);
    });
    $("#klikni6").click(function() {
    	$("#menu6").slideToggle(500);
    });
    
    
    
    $("form").on("submit", function(e) {
       e.preventDefault();
       var request = gapi.client.youtube.search.list({
            part: "snippet",
            type: "video",
            q: encodeURIComponent($("#search").val()).replace(/%20/g, "+"),
            maxResults: 2,
            
       }); 
       request.execute(function(response) {
          var rezultati = response.result;
          $("#rezultati").html("");
          $.each(rezultati.items, function(index, item) {
            $.get("knjiznice/okno.html", function(data) {
                $("#rezultati").append(prikazovalnik(data, [{"title":item.snippet.title, "videoid":item.id.videoId}]));
            });
          });
          ponastavitevVelikost();
       });
    
    $(window).on("resize", ponastavitevVelikost);
	});
});

function prikazovalnik(e,t){res=e;for(var n=0;n<t.length;n++){res=res.replace(/\{\{(.*?)\}\}/g,function(e,r){return t[n][r]})}return res}

function ponastavitevVelikost() {
    $(".video").css("height", $("#rezultati").width() * 9/16);
}

function start() {
	gapi.client.setApiKey("AIzaSyCLsuJ77LteeogodHkz1PRM_-bSSq2o_JM");
	gapi.client.load("youtube", "v3", function() {
		
	});
}